use <Hejkino2.2.scad>;

boardSize = [60 +1.2, 50, 15];
boardThickness = 1.6;
boardDrillD = 3;
boardBottomSpace = 3;

screwH = 5;
screwDrillD = 2.5;
screwDrillH = screwH -boardThickness;

boxThickness = 1.5;
boxGap = 0.5;
boxCornerRadius = 1.5;
boxHeight = boardSize[2] +screwH +2*boxGap +2*boxThickness;

magnetD = 2.5;
magnetH = 1;

//$vpt = [ 33.00, 26.50, 11.60 ];
//$vpr = [ 90.00, 0.00, 0.00 ];
$vpd = 60;

*union() { // v.1
    translate([boxThickness+boxGap, boxThickness+boxGap, screwH+boxThickness]) 
    board();
    box();
}

*union() { // v.2
    translate([boxThickness+boxGap, boxThickness+boxGap, boxThickness +boardBottomSpace]) 
    board();
    box2();
}

module box2() {
    ethConnSize = [21 +boxGap, 16 +boxGap, 13.7];
    pwrConnSize = [14.7 +boxGap, 8.8 +boxGap, 10.8];
    screwCenter = [1, boardSize[1]/2 +boxGap +boxThickness, boxHeight -10];
    
    
	roundedBox(boardSize, h=boxThickness);
    
    difference() {
        union() {
            roundedBox(boardSize, h=boxHeight, hole=true);
            translate(screwCenter) {
                cylinder(d=6, h=10);
                translate([0,0,-5]) cylinder(d1=0, d2=6, h=5);
            }
        }
   
        translate([screwCenter[0], screwCenter[1], boxHeight -7.99]) cylinder(d=2.5, h=8);
        
        translate([
            52.070 +10, 
            14.605 +boxGap +boxThickness, 
            ethConnSize[2]/2 +boxThickness +boardBottomSpace +boardThickness]
        ) cube(ethConnSize, center=true);
        
        translate([
            52.070 +10, 
            27.305 +boxGap +boxThickness, 
            pwrConnSize[2]/2 +boxThickness +boardBottomSpace +boardThickness]
        ) cube(pwrConnSize, center=true);
    }
    
    drillMove = boxThickness +1 +boxGap +boardDrillD/2;
    drillSpaceH = 55; 
    drillSpaceV = 45;
    
    for(i = [
        [drillMove, drillMove, 0], 
        [drillMove +drillSpaceH, drillMove, 0], 
        [drillMove, drillMove +drillSpaceV, 0], 
        [drillMove +drillSpaceH, drillMove +drillSpaceV, 0]
    ]) {
        translate(i) {
            cylinder(d=boardDrillD -boxGap, h=screwH +boxThickness);
            cylinder(d=boardDrillD*2, h=boxThickness +boardBottomSpace);
        }
    }
}

//translate([0, 60, 0]) 
lid();

module lid() {
	lidThickness = 0.6;
	lidHeight = 4.5;
	lidSize = [
		boardSize[0] +2*boxThickness +2*boxGap, 
		boardSize[1] +2*boxThickness +2*boxGap
	];
	trMove = boxThickness +boxGap;

	difference() {
		union() {
			translate([-trMove, -trMove, 0]) {
				color("lightblue") roundedBox(lidSize, h=lidThickness);
				roundedBox(lidSize, h=lidHeight, hole=true);
			}
			
			translate([trMove -boxGap/2, trMove -boxGap/2, 0]) roundedBox([
				boardSize[0] -2*boxThickness -boxGap, 
				boardSize[1] -2*boxThickness -boxGap
			], h=lidHeight, hole=true);
		}
		
		translate([2*boxGap, lidSize[1]/2, lidThickness]) {
			cylinder(d=2*boardDrillD +2*boxGap, h=boxThickness +boardBottomSpace);
			translate([0,0,-5]) cylinder(d=3, h=10);
		}
		
		*translate([0, lidSize[1] , boxHeight -1])
		rotate([180, 0, 0]) 
		%box2();
	}
}

module box() {
    ethConnSize = [21 +boxGap, 16 +boxGap, 13.7];
    pwrConnSize = [14.7 +boxGap, 8.8 +boxGap, 10.8];
    
    roundedBox(boardSize, h=boxThickness);
    
    difference() {
        roundedBox(boardSize, h=boxHeight, hole=true);
        
        translate([
            52.070 +10, 
            14.605 +boxGap +boxThickness, 
            ethConnSize[2]/2 +boxThickness +screwH +boardThickness]
        ) cube(ethConnSize, center=true);
        
        translate([
            52.070 +10, 
            27.305 +boxGap +boxThickness, 
            pwrConnSize[2]/2 +boxThickness +screwH +boardThickness]
        ) cube(pwrConnSize, center=true);
    }
    
    drillMove = boxThickness +1 +boxGap +boardDrillD/2;
    drillSpaceH = 55; 
    drillSpaceV = 45;
    
    for(i = [
        [drillMove, drillMove, 0], 
        [drillMove +drillSpaceH, drillMove, 0], 
        [drillMove, drillMove +drillSpaceV, 0], 
        [drillMove +drillSpaceH, drillMove +drillSpaceV, 0]
    ]) {
        translate(i) {
            difference() {
                cylinder(d1=boardDrillD +2, d2=boardDrillD, h=screwH +boxThickness);
                translate([0, 0, boxThickness +screwH -screwDrillH +0.001])
                cylinder(d=screwDrillD, h=screwDrillH);
            }
        }
    }
}      

module roundedBox(rect, h, hole=false) {
    linear_extrude(height=h)
    difference() {
        translate([boxCornerRadius, boxCornerRadius, 0])
        offset(r=boxCornerRadius) square([
            rect[0] +2*boxGap +2*boxThickness -2*boxCornerRadius, 
            rect[1] +2*boxGap +2*boxThickness -2*boxCornerRadius]
        );
        
        if (hole) {
            translate([boxThickness, boxThickness, 0])
            square([rect[0] +2*boxGap, rect[1] +2*boxGap]);
        }
    }
}

$fn = 100;